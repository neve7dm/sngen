from sngen import SNset


def test_unique_chars_in_the_charset():
    snset = SNset(0, 1, "abcda")
    assert snset.char_set == set("abcd")


def test_qty_requested_is_qty_generated():
    quantity = 2
    snset = SNset(quantity, 1)
    assert quantity == snset.generated


# TODO: add this test once there's a way to compare the uniqueness.
# def test_generated_sn_are_uniques():
#    snset = SNset(10, 3, "abcd")
