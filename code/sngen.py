#!/usr/bin/env python3

import csv
import sys
import time

ALPHABET_STRING = 'abcdefghijklmnopqrstuvwxyz'
ALPHANUMERIC_STRING = ALPHABET_STRING + '0123456789'

if not sys.argv[2:]:
    raise RuntimeError('! usage: ./sngen.py quantity size charset')


class SNset():
    def __init__(self, quantity, SN_length, char_set=ALPHANUMERIC_STRING):
        self.quantity = int(quantity)
        self.length = int(SN_length)
        self.char_set = set(char_set)
        self.generated = 0
        self.max = len(self.char_set)**self.length
        self.sn_set = set()
        self.warn_about_inputs()
        CSV_FILENAME = f'{quantity} SN - length {SN_length} - {char_set}.csv'
        with open(CSV_FILENAME, 'w') as csv_file:
            writer = csv.DictWriter(csv_file, ['SN'])
            writer.writeheader()
            self.generate(self.length, writer)

    def warn_about_inputs(self):
        if len(self.char_set) < len(self.char_set):
            print('! there\'s at least one char repeated in the charset')
        if self.max < self.quantity:
            print(f'''! Quantity asked will be limited to {self.max}.\n
                        One of both of following parameter should be increased:
                        SN length : {self.length},
                        or Charset length : {len(self.char_set)},
                        to get the required {self.quantity} serial numbers.
                        ''')

    # TODO : write tests to ensure quality!
    def generate(self, length, writer, sn=''):
        ''' generates recursively the serial numbers '''
        if length == 0 and self.generated < self.quantity:
            self.generated += 1
            if self.generated % 1000000 == 0:
                print(self.generated*100//self.quantity,
                      '% done',
                      ', last sn generated:',
                      sn,
                      )
            writer.writerow({'SN': sn})
            # print(f'{self.generated} : {sn}')
        elif self.generated < self.quantity:
            for char in self.char_set:
                self.generate(length-1, writer, sn+char)


if __name__ == '__main__':
    t0 = time.time()
    if len(sys.argv) == 4:
        new_set = SNset(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        new_set = SNset(sys.argv[1], sys.argv[2])
    t = time.time() - t0

    thousand_codes_per_s = (new_set.generated/1000)/t
    print(f'time to complete : {t}s, or {thousand_codes_per_s}K codes/s')
