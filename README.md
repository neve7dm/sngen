Welcome to the Serial Number generator.

This generator allows you to generate unique serial numbers
from the command line interface using the following command:
./sngen.py QUANTITY SIZE CHARSET

For example:
./sngen.py 100 5 abc will generate 100 serial numbers of size 5 using abc